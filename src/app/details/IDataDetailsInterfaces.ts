// coments

export interface IComentItems {
  snippet: {
    topLevelComment: {
      snippet: {
        authorDisplayName: string;
        publishedAt: string;
        authorProfileImageUrl: string;
        textDisplay: string;
      };
    };
  };
}

// video
export interface IVideo {
  snippet: {
    title: string;
  };
  statistics: {
    viewCount: string;
  };
}
