import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { YouTubeApiService } from '../YouTubeApiService';
import { DataManagement } from '../share/data-management';
import { IComentItems, IVideo } from './IDataDetailsInterfaces';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  template: string;
  _idVideo!: string;
  videoManagement: DataManagement<IVideo>;
  comentsManagement: DataManagement<IComentItems>;
  isScrolled = false;

  constructor(
    private _route: ActivatedRoute,
    private http: HttpClient,
    private youtube: YouTubeApiService
  ) {
    this.comentsManagement = new DataManagement();
    this.videoManagement = new DataManagement();
    this.template = '';
  }

  @HostListener('window:scroll')
  onScrollEvent() {
    if (this.isScrolled) {
      return;
    }
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
      this.isScrolled = true;
      this.nextResult();
    }
  }

  setIframe() {
    const id = this._route.snapshot.paramMap.get('id');
    const url = `http://www.youtube.com/embed/${id}?autoplay=1&origin=http://example.com`;

    this.template = `
    <div style="position:relative;height:0;padding-bottom:56.25%">
      <iframe style="position:absolute; width: 100%;height:100%;left:0"  type="text/html" width="640" height="360"
        src="${url}"
        frameborder="0" allowfullscreen/>
    </div>
  `;
  }

  getVideo() {
    this.youtube.getVideos(this._idVideo).subscribe((data: any) => {
      this.videoManagement.collection = data.items;
    });
  }

  getComents() {
    this.youtube.getCommentThreads(this._idVideo).subscribe(
      (data: any) => {
        this.comentsManagement.nextPageToken = data.nextPageToken;
        this.comentsManagement.collection = data.items;
        console.log('c', this.comentsManagement.collection);
      },
      err => {
        console.log('error get comments:', err);
      },
      () => {
        this.isScrolled = false;
      }
    );
  }

  nextResult() {
    if (!this.comentsManagement.nextPageToken || this.comentsManagement.nextPageToken === '') {
      return;
    }

    this.youtube.getCommentThreads(this._idVideo, this.comentsManagement.nextPageToken).subscribe(
      (data: any) => {
        this.comentsManagement.nextPageToken = data.nextPageToken;
        this.comentsManagement.add(data.items);
      },
      () => {
        this.isScrolled = false;
      },
      () => {
        this.isScrolled = false;
      }
    );
  }

  ngOnInit() {
    this._idVideo = this._route.snapshot.paramMap.get('id') || '';
    this.setIframe();
    this.getVideo();
    this.getComents();
  }
}
