import { Component, ElementRef, forwardRef, HostListener, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ISelect } from '../IMainSection';

@Component({
  selector: 'app-select-list',
  templateUrl: './select-list.component.html',
  styleUrls: ['./select-list.component.scss'],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SelectListComponent), multi: true }
  ]
})
export class SelectListComponent implements ControlValueAccessor {
  @Input() selectData: ISelect = {
    icon_button_select: '',
    select_options: []
  };

  value: any;
  isListVisible = false;
  isSelected: number = 0;
  propagateChange: any = () => {};

  writeValue(value: any): void {
    if (value !== undefined) {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void {}

  constructor(private elementRef: ElementRef) {

  }

  @HostListener('document:click', ['$event'])
  clickout(event: MouseEvent): void {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.isListVisible = false;
    }
  }

  selectItem(item: any): void {
    this.value = item;
    this.propagateChange(this.value);
    this.isListVisible = false;
  }

  getCssForSelected(option: any): boolean {
    return this.value && this.value.text === option.text;
  }

  toggleList(): void {
    this.isListVisible = !this.isListVisible;
  }
}
