import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { YouTubeApiService } from './YouTubeApiService';
import { FavoriteComponent } from './favorite/favorite.component';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { SelectListComponent } from './select-list/select-list.component';
import { DetailsComponent } from './details/details.component';
import { SanitizerHtmlPipe } from './share/pipe/sanitizer-html.pipe';
import { ShortTextPipe } from './share/pipe/short-text.pipe';
import { SearchComponent } from './search/search.component';





const routes: Routes = [
  {path: '', component: MainComponent},
  {path: 'favorite', component: FavoriteComponent},
  {path: 'details/:id', component: DetailsComponent},
  {path: '**', component: MainComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    FavoriteComponent,
    MainComponent,
    SelectListComponent,
    DetailsComponent,
    SanitizerHtmlPipe,
    ShortTextPipe,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [YouTubeApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
