export interface ISelectOption {
  text: string;
}
export interface ISelect {
  icon_button_select: string;
  select_options: ISelectOption[];
}

export interface IMainSection {
  input_text_area: string;
  icon_search: string;
  arrow: string;
  select: ISelect;
}

// data video interface
interface ISAnippetProperty {
  channelTitle: string;
  title: string;
  publishedAt: string;
  thumbnails: {
    medium: {
      url: string;
    }
  };
}

export interface IMainVideo {
  snippet: ISAnippetProperty;
}

export interface IEvenQuery {
  query: string,
  order: string
}
