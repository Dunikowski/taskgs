import { Component, OnInit } from '@angular/core';
import { YouTubeApiService } from '../YouTubeApiService';
import { Router } from '@angular/router';
import { WrapLocalStorage } from '../share/wrap-localstorage';
import { DataManagement} from '../share/data-management';
import { IMainVideo } from '../IMainSection';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.scss']
})

export class FavoriteComponent implements OnInit {
  localStorage: WrapLocalStorage;
  videoId_colection!: string[];
  favoriteDataManagement: DataManagement<IMainVideo>;
  localStorageKey: string;

  constructor(private youtube: YouTubeApiService, private router: Router) {
    this.localStorage = new WrapLocalStorage();
    this.favoriteDataManagement = new DataManagement();
    this.localStorageKey = environment.favoriteList || '';
  }

  ngOnInit() {
    if (this.localStorage.get(this.localStorageKey) !== null) {
      this.videoId_colection = this.localStorage.get(this.localStorageKey ) || [];
    } else {
      this.router.navigate(['']);
    }

    const colection = this.videoId_colection.join();
    
    this.youtube
      .getVideos(colection)
      .subscribe((data: any) => {
        this.favoriteDataManagement.collection = data.items;
      });
  }

  removeFavorite(index: number): void {
    this.videoId_colection.splice(index, 1);
    this.localStorage.removeFavorite(this.localStorageKey );

    if (this.videoId_colection.length > 0) {
      this.localStorage.set(this.localStorageKey , this.videoId_colection);
    } else {
      this.router.navigate(['/main']);
    }

    this.videoId_colection = this.localStorage.get(this.localStorageKey ) || [];
    let param = '';
    const j = this.videoId_colection;
    if (j !== null) {
      if (j.length > 1) {
        param = this.videoId_colection.join();
      } else {
        param = this.videoId_colection[0];
      }
    }

    this.youtube.getVideos(param).subscribe((data: any) => {
      this.favoriteDataManagement.collection = data.items;
    });
  }

  details(item: any): void {
    this.router.navigate(['/details', item.id]);
  }
}
