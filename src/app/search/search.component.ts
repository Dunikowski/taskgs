import { Component, OnInit, Input, Output } from '@angular/core';

import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IMainSection, ISelect, IMainVideo, IEvenQuery  } from '../IMainSection';
import { DataManagement } from '../share/data-management';
import { mainSection } from '../../assets/data';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit { 

@Input() count_favorit: string ='';
@Output() query = new EventEmitter<IEvenQuery>();

    _selectData!: ISelect;
    dataNvBar: IMainSection;
    mainVideoManagement: DataManagement<IMainVideo>;
    myForm: FormGroup;
    search: string = 'Search';
  
    long_input = false;
    
    constructor(
      private router: Router,
      private _fb: FormBuilder
    ) {
    
      this.mainVideoManagement = new DataManagement<IMainVideo>();
      this.dataNvBar = mainSection;
      this._selectData = mainSection.select;
      this.myForm = this._fb.group({
        search: '',
        _select: mainSection.select.select_options[0]
      });
      
    }
   
    ngOnInit() {
      this.myForm.get('_select')?.valueChanges.subscribe(values => {
        const query = this._query(this.myForm.value.search);
        const order = values.text.toString().toLowerCase();
        this.query.emit({
          query,
          order
        });
        
      });
      this.send();
    }
   
    longInput(): void {
      this.long_input = !this.long_input;
    }
  
    details(item: any): void {
      this.router.navigate(['/details', item.id.videoId]);
    }
  
    private _query(q: string): string {
      const condition = /[\s]+/g;
      const i = q.trim().replace(condition, '+');
      return i;
    }

    send(): void {
      const query = this._query(this.myForm.value.search);
      const order = this.myForm.value._select.text.toString().toLowerCase();
    
      this.query.emit({
        query,
        order
      });
    }
}
