import { Component, OnInit, HostListener } from "@angular/core";
import { YouTubeApiService } from "../YouTubeApiService";
import { Router } from "@angular/router";
import { IMainSection, ISelect, IMainVideo, IEvenQuery } from "../IMainSection";
import { WrapLocalStorage } from "../share/wrap-localstorage";
import { DataManagement } from "../share/data-management";
import { mainSection } from "../../assets/data";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.scss"],
})
export class MainComponent implements OnInit {
  _selectData!: ISelect;
  count_favorit: string;
  search = "Search";
  dataNvBar: IMainSection;
  mainVideoManagement: DataManagement<IMainVideo>;
  _LocalStorage: WrapLocalStorage;
  videoId_colection!: string[];
  isScrolled = false;
  long_input = false;
  localStorageKey!: string;
  order: string = "RELEVANCE";
  query: string = "";

  constructor(private youtube: YouTubeApiService, private router: Router) {
    this._LocalStorage = new WrapLocalStorage();
    this.localStorageKey = environment.favoriteList || "";
    this.count_favorit = this._LocalStorage.coutItems(this.localStorageKey);
    this.mainVideoManagement = new DataManagement<IMainVideo>();
    this.dataNvBar = mainSection;
  }

  @HostListener("window:scroll")
  onScrollEvent() {
    if (!this.isScrolled) {
      if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        this.isScrolled = true;
        this.nextResult();
      }
    } else {
      return;
    }
  }

  ngOnInit() {
    this.countFavorite();
    this._selectData = mainSection.select;

    this.show({
      query: this.query,
      order: this.order,
    });
  }

  addFavorite(item: any): void {
    if (this._LocalStorage.isLocalStorage) {
      this.videoId_colection =
        this._LocalStorage.get(this.localStorageKey) || [];
      this.videoId_colection.push(item.id.videoId);
      this._LocalStorage.set(this.localStorageKey, this.videoId_colection);
    }

    this.router.navigate(["/favorite"]);
  }

  countFavorite(): void {
    this.count_favorit = this._LocalStorage.coutItems(this.localStorageKey);
  }

  longInput(): void {
    this.long_input = !this.long_input;
  }

  details(item: any): void {
    this.router.navigate(["/details", item.id.videoId]);
  }

  show(data: IEvenQuery): void {
    const { query, order } = data;

    this.youtube.searchVideos(query, order).subscribe(
      (data: any) => {
        this.mainVideoManagement.nextPageToken = data.nextPageToken;
        this.mainVideoManagement.collection = data.items;
      },
      (err: any) => {
        console.log(err);
        this.isScrolled = false;
      },
      () => {
        this.isScrolled = false;
      }
    );
  }

  nextResult(): void {
    if (
      !this.mainVideoManagement.nextPageToken ||
      this.mainVideoManagement.nextPageToken === ""
    ) {
      return;
    }
    const _query = this.query;
    const _order = this.order;
    const _pageToken = this.mainVideoManagement.nextPageToken;
    this.youtube.searchVideos(_query, _order, _pageToken).subscribe(
      (data: any) => {
        this.mainVideoManagement.nextPageToken = data.nextPageToken;
        this.mainVideoManagement.add(data.items);
      },
      (err) => {
        console.log(err);
        this.isScrolled = false;
      },
      () => {
        this.isScrolled = false;
      }
    );
  }
}
