import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class YouTubeApiService {
  private apiKey: string = environment.apiKey || '';

  constructor(private http: HttpClient) {}

  getCommentThreads(videoId: string, pageToken: string = ''): Observable<any> {
    const url = environment.commentThreadsUrl || '';
    const params = this.getCommonParams()
      .set('videoId', videoId)
      .set('pageToken', pageToken);

    return this.http.get(url, { params });
  }

  getVideos(videoIds: string): Observable<any> {
    const url = environment.videosUrl || '';
    const params = this.getCommonParams().set('id', videoIds);

    return this.http.get(url, { params });
  }

  searchVideos(query: string, order: string, pageToken: string = ''): Observable<any> {
    const url = environment.searchUrl || '';
    const params = this.getCommonParams()
      .set('q', query)
      .set('order', order)
      .set('pageToken', pageToken);

    return this.http.get(url, { params });
  }

  private getCommonParams(): HttpParams {
    return new HttpParams()
      .set('part', 'snippet')
      .set('maxResults', '10')
      .set('textFormat', 'plainText')
      .set('key', this.apiKey);
  }
}
