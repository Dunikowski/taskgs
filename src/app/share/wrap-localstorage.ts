export class WrapLocalStorage {
  public isLocalStorage: boolean;

  constructor() {
    this.isLocalStorage = this.checkLocalStorage();
  }

 public checkLocalStorage(): boolean {
  const uid = new Date().toString();

  try {
    localStorage.setItem(uid, uid);
    const result = localStorage.getItem(uid) === uid;
    localStorage.removeItem(uid);
    return result;
  } catch (e) {
    return false;
  }
}


  public set(key: string, value: string[]): void {
    try {
      const _json = JSON.stringify(value);
      localStorage.setItem(key, _json);
    } catch (error) {
      console.error(error);
    }
  }

  public get(key: string): string[] | null {
    try {
      const collection = localStorage.getItem(key);
      return collection ? JSON.parse(collection) : null;
    } catch (error) {
      console.error(error);
      return null;
    }
  }
  

  public removeFavorite(key: string): void {
    try {
      localStorage.removeItem(key);
    } catch (error) {
      console.error(error);
    }
  }

  public coutItems(key: string): string {
    const _jsonItems = this.get(key);
    return _jsonItems ? _jsonItems.length.toString() : '0';
  }
}

