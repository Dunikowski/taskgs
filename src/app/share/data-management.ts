export class DataManagement<T> {
  private _nextPageToken: string = '';
  private _items: T[] = [];

  constructor() {}

  get nextPageToken(): string | undefined {
    return this._nextPageToken;
  }

  set nextPageToken(value: string | undefined ) {
    this._nextPageToken = value || '';
  }

  get collection(): T[] {
    return this._items;
  }

  set collection(value: T[]) {
    this._items = value || [];
  }

  public add(value: T[]): void {
    this._items.push(...value);
  }
}

