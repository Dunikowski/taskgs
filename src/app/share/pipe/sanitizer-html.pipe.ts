import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({
  name: 'sanitizerHtml'
})
export class SanitizerHtmlPipe implements PipeTransform {
  constructor(protected _sanitizer: DomSanitizer) {}
  transform(value: any): SafeHtml {
        const i = this._sanitizer.bypassSecurityTrustHtml(value);
    return i;
  }

}
