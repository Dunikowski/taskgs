import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortText'
})
export class ShortTextPipe implements PipeTransform {
  transform(value: string, args?: string | number): string {
    const _value = value.trim();
    const _length = _value.length;
    const _setLength = (args !== null && args !== undefined) ? parseInt(args.toString(), 10) : 100;
    let result;
    let i;

    switch (true) {
      case _length <= _setLength:
        result = value;
        break;
      case _length > _setLength:
        i = _value.lastIndexOf(' ', _setLength);
        result = `${_value.substring(0, i)} ...`;
        break;
      default:
        result = _value;
    }

    return result;
  }
}
