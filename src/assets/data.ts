export const mainSection = {
  "input_text_area": "Items count:",
  "icon_search":"./assets/images/search.png",
  "icon_button_select":"./assets/images/wskaznik.png",
  "arrow": "./assets/images/arrow.png",
  "select":{
    "icon_button_select":"./assets/images/wskaznik.png",
    "select_options": [
      { "text": "Relevance"},
      { "text": "Title" },
      { "text": "Date"}
    ]
  }
}
